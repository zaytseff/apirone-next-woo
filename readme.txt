== Multi Cripto Currency Payments ==

Contributors: zaytseff
Tags: bitcoin, accept bitcoin, bitcoin payments, bitcoins, BTC, LTC, BCH, Doge, crypto, cryptocurrency, forwarding, payment, processing, acquiring, receive bitcoins, pay via cryptocurrency, crypto, bitcoin wallet
Requires PHP: 7.4
Requires at least: 5.6
Tested up to: 5.8.2
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

== Description ==

The fastest and easiest way to start accepting Crypto payments (Bitcoin, Litecoin, Bitcoin Cash, and Dogecoin) on your WooCommerce online store.
You don’t need to use third-party accounts.
Fully Anonymous payments.
White label plugin. Your customers pay in your store without redirects.
No KYC/ Docs or AML.
TOR network and IPv6 support.
All payments are instantly forwards to your specified addresses in BTC, LTC, BCH, Doge.
Support of all Woocommerce fiat currencies.
No extra fees. Just payment gateway fee + network fee.
Installation and fast start for 1 minute.

== Settings ==

Just specify your address for every cryptocurrency you want to get payments for.

*Screenshot of settings

== Payment process ==

The plugin generates a new address for each order. Payment gateway monitors a network and notifies the Crypto plugin about transactions.

Partial payments are acceptable.

After a specified number of confirmations and full amount arrives, an order will be complete.

The store owner can change the status of an order at any time, complete underpaid or
return the rest of overpaid orders manually. At the discretion of the owner.

*Screenshot of the payment process
